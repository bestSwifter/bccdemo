//
//  HTNetworkHeader.h
//  BCCDemo
//
//  Created by Hayato on 2018/11/27.
//  Copyright © 2018 Hayato. All rights reserved.
//

#ifndef HTNetworkHeader_h
#define HTNetworkHeader_h

#import "HTNetworkManager.h"
#import "HTRequest.h"
#import "HTResponse.h"
#import "HTJSONModel.h"

#endif /* HTNetworkHeader_h */
