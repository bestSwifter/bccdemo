//
//  HTBCCResponse.m
//  BCCDemo
//
//  Created by Hayato on 2018/11/27.
//  Copyright © 2018 Hayato. All rights reserved.
//

#import "HTBCCResponse.h"

@implementation HTCard : HTObject

- (NSString *)name {
    return [NSString stringWithFormat:@"%@%@", self.lastName, self.firstName];
}

@end

@implementation HTCardsData

+ (Class)classForArray:(NSString *)propertyName {
    if ([@"cards" isEqualToString:propertyName]) {
        return [HTCard class];
    }
    return [super classForArray:propertyName];
}

@end

@implementation HTBCCResponse

@end
