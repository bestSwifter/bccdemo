//
//  HTBCCResponse.h
//  BCCDemo
//
//  Created by Hayato on 2018/11/27.
//  Copyright © 2018 Hayato. All rights reserved.
//

#import "HTResponse.h"
#import "HTObject.h"

@interface HTCard : HTObject

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *company;

@property (nonatomic, copy) NSString *img;

@property (nonatomic, copy) NSString *firstName;

@property (nonatomic, copy) NSString *lastName;

@property (nonatomic, copy) NSString *department;

@property (nonatomic, copy, readonly) NSString *name;

@end

@interface HTCardsData : HTObject

@property(nonatomic, strong)NSArray *cards;

@end


@interface HTBCCResponse : HTResponse

@property (nonatomic, strong) HTCardsData *data;

@end


