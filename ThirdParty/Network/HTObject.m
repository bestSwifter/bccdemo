//
//  HTObject.m
//  EasyGames
//
//  Created by chris on 9/4/14.
//  Copyright (c) 2014 iEasynote. All rights reserved.
//

#import "HTObject.h"
#import "NSString+Utilities.h"

@interface HTObject (JSONModel)

- (NSDictionary *)jsonModelDictionary;

- (NSString *)jsonModelJson;

@end

@implementation HTObject


- (void) calculateContentHeight{
    
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (!dictionary) {
        return nil;
    }
    NSError *error;
    HTObject *object = [self initWithDictionary:dictionary error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    return object;
}

- (instancetype)initWithJSON:(NSString *)jsonString {
    if ([NSString isNullOrEmpty:jsonString]) {
        return nil;
    }
    NSError *error;
    HTObject *object = [self initWithString:jsonString error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    return object;
}

- (NSDictionary *)dictionary {
    return [self jsonModelDictionary];
}

- (NSString *)json {
    return [self jsonModelJson];
}

+ (Class)classForArray:(NSString *)propertyName {
    return nil;
}

+ (NSArray *)ignoredProperties {
    return nil;
}

+ (instancetype)itemWithDictionary:(NSDictionary <NSString *, id> *)dict {
    HTObject *item = [[self alloc] init];
    [item setValuesForKeysWithDictionary:dict];
    return item;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    NSLog(@"%@ serValueForUndefinedKey", self.class);
}

@end

@implementation HTObject (JSONModel)

+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

+ (BOOL)propertyIsIgnored:(NSString *)propertyName {
    NSArray *ignoredProperties = [self ignoredProperties];
    if (ignoredProperties && [ignoredProperties containsObject:propertyName]) {
        return YES;
    }
    return NO;
}

+ (NSString *)protocolForArrayProperty:(NSString *)propertyName {
    Class clazz = [self classForArray:propertyName];
    if (clazz) {
        return NSStringFromClass([clazz class]);
    }
    return nil;
}

- (NSDictionary *)jsonModelDictionary {
    return [super toDictionary];
}

- (NSString *)jsonModelJson {
    return [super toJSONString];
}

+ (NSDictionary *)convertKeyMapper {
    return nil;
}

+ (JSONKeyMapper *)keyMapper {
    if ([self convertKeyMapper]) {
        return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:[self convertKeyMapper]];
    }
    return nil;
}

@end
