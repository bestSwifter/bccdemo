//
//  HTObject.h
//  EasyGames
//
//  Created by chris on 9/4/14.
//  Copyright (c) 2014 iEasynote. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface HTObject : JSONModel

- (void)calculateContentHeight;

- (NSString *)json;

- (NSDictionary *)dictionary;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (instancetype)initWithJSON:(NSString *)jsonString;

+ (NSArray *)ignoredProperties;

+ (Class)classForArray:(NSString *)propertyName;

+ (instancetype)itemWithDictionary:(NSDictionary <NSString *, id> *)dict;

@end
