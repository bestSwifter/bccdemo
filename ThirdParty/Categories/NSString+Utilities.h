//
//  NSString+Utilities.h
//  Mars
//
//  Created by chris on 4/29/14.
//  Copyright (c) 2014 lifang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utilities)

+ (BOOL)isNullOrEmpty:(NSString*)str;

@end
