//
//  NSString+Utilities.m
//  Mars
//
//  Created by chris on 4/29/14.
//  Copyright (c) 2014 lifang. All rights reserved.
//


#import "NSString+Utilities.h"

@implementation NSString (Utilities)

+ (BOOL) isNullOrEmpty:(NSString*)str{
    return  !str || str==nil || (NSString*)[NSNull null]==str || [str isEqualToString:@""];
}

@end
