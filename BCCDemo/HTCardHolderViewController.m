//
//  HTCardHolderViewController.m
//  BCCDemo
//
//  Created by Hayato on 2018/11/27.
//  Copyright © 2018 Hayato. All rights reserved.
//

#import "HTCardHolderViewController.h"
#import "HTNetworkManager.h"
#import "HTRefreshTableView.h"
#import "UIScrollView+HTRefresh.h"
#import "HTRefreshType.h"
#import "HTRefreshIndicatorView.h"
#import "HTBCCRequest.h"
#import "HTBCCResponse.h"
#import "HTCardCell.h"

@interface HTCardHolderViewController ()< UITableViewDelegate, UITableViewDataSource, HTTableViewRefreshingDelegate>

@property (nonatomic, strong) HTRefreshTableView *tableView;

@property (nonatomic, strong) HTBCCResponse *response;

@end

@implementation HTCardHolderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadRemoteData];
    
    [self loadSubviews];
}

# pragma mark - remoteData
- (void)loadRemoteData {
    
    HTBCCRequest *request = [[HTBCCRequest alloc] init];
    __weak typeof(self) weakSelf = self;
    [[HTNetworkManager sharedManager] get:request forResponseClass:[HTBCCResponse class] success:^(HTResponse *response) {
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf handleLoadedRemoteData:(HTBCCResponse *)response];
    } failure:^(NSError *error) {
    
    }];
}

- (void)handleLoadedRemoteData:(HTBCCResponse *)response {
    self.response = response;
    [self.tableView reloadData];
}

# pragma mark - tableView & dataSource
- (void)loadSubviews {
    
    _tableView = [[HTRefreshTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped refreshType:HTRefreshTypeTop | HTRefreshTypeBottom];
    [_tableView setRefreshHeaderWithIndicatorClass:[HTRefreshIndicatorView class]];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.refreshDelegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.separatorInset = UIEdgeInsetsMake(0, 8, 0, 8);
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [self.view addSubview:_tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.response && self.response.data && self.response.data.cards && self.response.data.cards.count) {
        return self.response.data.cards.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * const cardCellReuseID = @"HTOCCCardCell";
    HTCardCell *cell = [tableView dequeueReusableCellWithIdentifier:cardCellReuseID];
    if (!cell) {
        cell = [[HTCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cardCellReuseID];
    }
    HTCard *card = nil;
    
    if (self.response && self.response.data && self.response.data.cards && self.response.data.cards.count > indexPath.row) {
        card = self.response.data.cards[indexPath.row];
    }
    [cell showWithData:card];
    return cell;
}

# pragma mark - tableViewRefreshingDelegate
- (void)beginRefreshHeader:(HTRefreshTableView *)tableView {
    
}

- (void)beginRefreshFooter:(HTRefreshTableView *)tableView{
    
}

@end
