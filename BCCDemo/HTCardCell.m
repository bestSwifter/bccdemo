//
//  HTCardCell.m
//  BCCDemo
//
//  Created by Hayato on 2018/11/27.
//  Copyright © 2018 Hayato. All rights reserved.
//

#import "HTCardCell.h"
#import "HTResponse.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "HTBCCResponse.h"

@interface HTCardCell ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *companyLabel;

@end

@implementation HTCardCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self loadContentView];
    }
    return self;
}

- (void)loadContentView {
    
    UIView *contentView = self.contentView;
    
    UIImageView *iconImageView = [[UIImageView alloc] init];
    [contentView addSubview:iconImageView];
    self.iconImageView = iconImageView;
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.font = [UIFont systemFontOfSize:20];
    [contentView addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    UILabel *titleLabel = [[UILabel alloc] init];
    [contentView addSubview:titleLabel];
    self.titleLabel = titleLabel;
    
    UILabel *companyLabel = [[UILabel alloc] init];
    [contentView addSubview:companyLabel];
    self.companyLabel = companyLabel;
    
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.mas_equalTo(contentView).offset(10);
        make.bottom.mas_equalTo(contentView).offset(-10);
        make.width.mas_equalTo(80);
    }];
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(iconImageView.mas_top);
        make.leading.mas_equalTo(iconImageView.mas_trailing).offset(10);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(nameLabel.mas_leading);
        make.top.mas_equalTo(nameLabel.mas_bottom).offset(10);
    }];
    
    [companyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(nameLabel.mas_leading);
        make.bottom.mas_equalTo(iconImageView.mas_bottom);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)showWithData:(HTObject *)data {
    
    HTCard *card = (HTCard *)data;
    
    if (!card) { // clear cache
        [self.iconImageView sd_setImageWithURL:nil];
        self.nameLabel.text = @"";
        self.titleLabel.text = @"";
        self.companyLabel.text = @"";
        return;
    }
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:card.img]];
    self.nameLabel.text = card.name;
    self.titleLabel.text = card.department;
    self.companyLabel.text = card.company;
}

@end
